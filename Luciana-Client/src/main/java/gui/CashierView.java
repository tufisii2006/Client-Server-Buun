package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;

import model.Product;
import model.User;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JComboBox;
import javax.swing.JTextPane;
import javax.swing.table.DefaultTableModel;

import clientServerCommunication.SendInfoToServer;

import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CashierView {

	private JFrame frame;
	private User sesionUser;
	private JTable table_products;
	private JTextField textField_idprod;
	private JTextField textField_quantity;
	JScrollPane scrollPane = new JScrollPane();
	private JTextField textField_newUpdateQuantity;
	private String order_description = new String();
	private List<Product> newOrderProducts = new LinkedList<Product>();
	List<Object> allProducts = new ArrayList<Object>();
	private double total = 0;
	int indexListaNoua = 0;

	public CashierView(User sesionUser) throws UnknownHostException, IOException {
		this.sesionUser = sesionUser;
		ImageIcon iconAddItem = new ImageIcon(this.getClass().getResource("/b.png"));
		ImageIcon iconFinishOrder = new ImageIcon(this.getClass().getResource("/f.png"));

		ImageIcon iconAdd = new ImageIcon(this.getClass().getResource("/add.png"));
		ImageIcon iconDelete = new ImageIcon(this.getClass().getResource("/delete.png"));
		ImageIcon iconUpdate = new ImageIcon(this.getClass().getResource("/update.png"));
		String command = new String("GetAllProducts");
		List<Object> toSend = new ArrayList<Object>();
		toSend.add(command);
		allProducts = SendInfoToServer.processInformation(toSend);
		initialize();
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(null);

		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 37, 77, 22);
		frame.getContentPane().add(lblName);

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(10, 70, 77, 22);
		frame.getContentPane().add(lblUsername);

		scrollPane.setBounds(10, 171, 396, 361);
		frame.getContentPane().add(scrollPane);

		scrollPane.setViewportView(table_products);

		JLabel lblAllProducts = new JLabel("All products");
		lblAllProducts.setBounds(139, 138, 103, 22);
		frame.getContentPane().add(lblAllProducts);

		final JTextPane textPane = new JTextPane();
		textPane.setBounds(647, 103, 337, 367);
		frame.getContentPane().add(textPane);

		JLabel lblFacturaNoua = new JLabel("New Order");
		lblFacturaNoua.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFacturaNoua.setBounds(767, 11, 116, 18);
		frame.getContentPane().add(lblFacturaNoua);

		JLabel lblIdproduct = new JLabel("IDProduct");
		lblIdproduct.setBounds(647, 41, 77, 14);
		frame.getContentPane().add(lblIdproduct);

		textField_idprod = new JTextField();
		textField_idprod.setBounds(713, 38, 86, 20);
		frame.getContentPane().add(textField_idprod);
		textField_idprod.setColumns(10);

		JLabel lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(647, 74, 46, 14);
		frame.getContentPane().add(lblQuantity);

		textField_quantity = new JTextField();
		textField_quantity.setBounds(713, 71, 86, 20);
		frame.getContentPane().add(textField_quantity);
		textField_quantity.setColumns(10);

		JButton btnUpdateStock = new JButton("Update Stock");
		btnUpdateStock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Product product = new Product();
					int k = table_products.getSelectedRow();
					product.setId((Integer) table_products.getModel().getValueAt(k, 0));
					product.setStock(Integer.parseInt(textField_newUpdateQuantity.getText()));
					product.setDescription((String) table_products.getModel().getValueAt(k, 2));
					product.setName((String) table_products.getModel().getValueAt(k, 1));
					product.setPrice((Double) table_products.getModel().getValueAt(k, 3));
					List<Object> toSend = new ArrayList<Object>();
					String command = new String("UpdateProductByCashier");
					toSend.add(command);
					toSend.add(product);
					SendInfoToServer.processInformation(toSend);
					update_products();
					textField_newUpdateQuantity.setText("");
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnUpdateStock.setIcon(iconUpdate);
		btnUpdateStock.setBounds(92, 569, 176, 23);
		frame.getContentPane().add(btnUpdateStock);

		JLabel lblNume = new JLabel(this.sesionUser.getName());
		lblNume.setBackground(Color.RED);
		lblNume.setBounds(63, 37, 103, 18);
		frame.getContentPane().add(lblNume);

		JLabel lblUN = new JLabel(this.sesionUser.getUsername());
		lblUN.setBackground(Color.RED);
		lblUN.setBounds(84, 72, 93, 18);
		frame.getContentPane().add(lblUN);

		JLabel lblNewQuantity = new JLabel("New Quantity");
		lblNewQuantity.setBounds(41, 540, 93, 18);
		frame.getContentPane().add(lblNewQuantity);

		textField_newUpdateQuantity = new JTextField();
		textField_newUpdateQuantity.setBounds(139, 539, 86, 20);
		frame.getContentPane().add(textField_newUpdateQuantity);
		textField_newUpdateQuantity.setColumns(10);
		final JLabel lblOrderTotal = new JLabel("");
		lblOrderTotal.setBounds(767, 485, 94, 22);
		frame.getContentPane().add(lblOrderTotal);

		JButton btnAddItem = new JButton("Add item");
		btnAddItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Product prod = new Product();
				for (Object p : allProducts) {
					if (((Product) p).getId() == Integer.parseInt(textField_idprod.getText())) {
						newOrderProducts.add((Product) p);
						prod = (Product) p;
					}
				}
				if (Integer.parseInt(textField_quantity.getText()) > prod.getStock()
						|| Integer.parseInt(textField_quantity.getText()) < 0) {
					JOptionPane.showMessageDialog(frame,
							"There are not enought products of this type !!Chose lower quantity", "Add ERROR",
							JOptionPane.ERROR_MESSAGE);
				} else {
					int old_stock = prod.getStock();
					prod.setStock(old_stock - Integer.parseInt(textField_quantity.getText()));
					order_description = order_description + "\r\n" + prod.getName() + " X"
							+ textField_quantity.getText();
					textPane.setText(order_description);

					total = total + newOrderProducts.get(indexListaNoua).getPrice()
							* Integer.parseInt(textField_quantity.getText());
					indexListaNoua++;
					lblOrderTotal.setText(String.valueOf(total) + " RON");
					textField_idprod.setText("");
					textField_quantity.setText("");
				}

			}
		});
		btnAddItem.setIcon(iconAddItem);
		btnAddItem.setBounds(833, 53, 126, 23);
		frame.getContentPane().add(btnAddItem);

		JButton btnFinalizeazaComanda = new JButton("Finalizeaza comanda");
		btnFinalizeazaComanda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					String command = new String("FinishOrder");
					List<Object> toSend = new ArrayList<Object>();
					toSend.add(command);
					for (Product p : newOrderProducts) {
						toSend.add(p);
						System.out.println("item" + p.getName());
					}
					SendInfoToServer.processInformation(toSend);
					newOrderProducts.clear();
					total = 0;
					indexListaNoua = 0;
					update_products();
					lblOrderTotal.setText("");
					textPane.setText("");
				} catch (UnknownHostException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnFinalizeazaComanda.setBounds(723, 525, 200, 33);
		btnFinalizeazaComanda.setIcon(iconFinishOrder);
		frame.getContentPane().add(btnFinalizeazaComanda);

		JLabel lblTotal = new JLabel("Total:");
		lblTotal.setBounds(713, 493, 46, 14);
		frame.getContentPane().add(lblTotal);

		update_products();
	}

	private void initialize() {
		frame = new JFrame(this.sesionUser.getName());
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		frame.setBounds(100, 100, 1032, 632);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void update_products() throws UnknownHostException, IOException {

		String[] columns_prod = { "Id", "Name", "Description", "Price", "Stock", };
		Object[][] dataprod = null;
		JTable table_products = new JTable(new DefaultTableModel(dataprod, columns_prod));
		table_products.setFont(new Font("Arial", Font.ITALIC, 12));
		table_products.setBackground(Color.LIGHT_GRAY);
		table_products.setForeground(Color.BLUE);
		table_products.setColumnSelectionAllowed(true);
		table_products.setCellSelectionEnabled(true);
		DefaultTableModel model_soferi_table = (DefaultTableModel) table_products.getModel();

		String command = new String("GetAllProducts");
		List<Object> toSend = new ArrayList<Object>();
		toSend.add(command);
		List<Object> allProducts = new ArrayList<Object>();

		allProducts = SendInfoToServer.processInformation(toSend);

		for (int i = 0; i < allProducts.size(); i++) {
			model_soferi_table.addRow(new Object[] { ((Product) allProducts.get(i)).getId(),
					((Product) allProducts.get(i)).getName(), ((Product) allProducts.get(i)).getDescription(),
					((Product) allProducts.get(i)).getPrice(), ((Product) allProducts.get(i)).getStock() });
		}
		table_products.setModel(model_soferi_table);
		this.table_products = table_products;
		Font f = new Font("Arial", Font.BOLD, 14);
		table_products.getTableHeader().setFont(f);
		scrollPane.setViewportView(table_products);
	}
}
