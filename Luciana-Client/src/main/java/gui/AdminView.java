package gui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import clientServerCommunication.SendInfoToServer;
import controler.LoginControler;
import model.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import java.awt.SystemColor;
import java.io.IOException;
import java.net.UnknownHostException;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;

public class AdminView {

	private JFrame frame;
	private JTable table_products;
	private JTable table_clients;
	JScrollPane scrollPane_products = new JScrollPane();
	JScrollPane scrollPane_employees = new JScrollPane();
	JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	private JTextField textField_name_prod;
	private JTextField textField_descr_prod;
	private JTextField textField_price;
	private JTextField textField_stock;
	private JTextField textField_update_id;
	private JTextField textField_employee_name;
	private JTextField textField_employee_username;
	private JTextField textField_employee_password;
	private JTextField textField_role;
	private JTextField textField_employe_ID_update;

	private User sesionUser;

	public AdminView(User sesionUser) throws UnknownHostException, IOException {
		this.sesionUser = sesionUser;
		initialize();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);

	}

	private void initialize() throws UnknownHostException, IOException {
		ImageIcon iconAdd= new ImageIcon(this.getClass().getResource("/add.png"));	
		ImageIcon iconDelete= new ImageIcon(this.getClass().getResource("/delete.png"));	
		ImageIcon iconUpdate= new ImageIcon(this.getClass().getResource("/update.png"));	
		ImageIcon iconAdmin= new ImageIcon(this.getClass().getResource("/admin.png"));	
		ImageIcon iconPdf= new ImageIcon(this.getClass().getResource("/pdfmic.png"));	
		
		frame = new JFrame("Admin View");
		frame.setForeground(Color.MAGENTA);
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		frame.setBackground(new Color(0, 204, 255));
		frame.setBounds(100, 100, 1058, 684);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		tabbedPane.setBackground(Color.YELLOW);

		tabbedPane.setBounds(10, 122, 537, 469);
		frame.getContentPane().add(tabbedPane);

		table_products = new JTable();
		table_products.setFont(new Font("Arial", Font.ITALIC, 12));
		table_products.setBackground(Color.LIGHT_GRAY);
		table_products.setForeground(Color.BLUE);
		table_products.setColumnSelectionAllowed(true);
		table_products.setCellSelectionEnabled(true);
		tabbedPane.addTab("Products", null, scrollPane_products, null);
		table_clients = new JTable();
		table_clients.setFont(new Font("Arial", Font.ITALIC, 12));
		table_clients.setBackground(Color.LIGHT_GRAY);
		table_clients.setColumnSelectionAllowed(true);
		table_clients.setCellSelectionEnabled(true);
		table_clients.setForeground(Color.BLUE);
		tabbedPane.addTab("Employees", null, scrollPane_employees, null);
		scrollPane_products.setViewportView(table_products);
		scrollPane_employees.setViewportView(table_clients);

		JLabel numeSesionUser = new JLabel(this.sesionUser.getName());
		numeSesionUser.setIcon(iconAdmin);
		numeSesionUser.setBounds(441, 28, 325, 83);
		frame.getContentPane().add(numeSesionUser);

		JLabel lblWellcome = new JLabel("Wellcome");
		lblWellcome.setForeground(Color.BLUE);
		lblWellcome.setFont(new Font("Yu Gothic", Font.BOLD | Font.ITALIC, 16));
		lblWellcome.setBounds(153, 32, 187, 72);
		frame.getContentPane().add(lblWellcome);

		JLabel lblName = new JLabel("Name");
		lblName.setBounds(573, 159, 46, 14);
		frame.getContentPane().add(lblName);

		JLabel lblDescription = new JLabel("Description");
		lblDescription.setBounds(569, 184, 62, 14);
		frame.getContentPane().add(lblDescription);

		JLabel lblPrice = new JLabel("Price");
		lblPrice.setBounds(573, 215, 46, 14);
		frame.getContentPane().add(lblPrice);

		JLabel lblStock = new JLabel("Stock");
		lblStock.setBounds(573, 240, 46, 14);
		frame.getContentPane().add(lblStock);

		textField_name_prod = new JTextField();
		textField_name_prod.setBounds(634, 156, 101, 20);
		frame.getContentPane().add(textField_name_prod);
		textField_name_prod.setColumns(10);

		textField_descr_prod = new JTextField();
		textField_descr_prod.setBounds(634, 181, 101, 20);
		frame.getContentPane().add(textField_descr_prod);
		textField_descr_prod.setColumns(10);

		textField_price = new JTextField();
		textField_price.setBounds(634, 212, 101, 20);
		frame.getContentPane().add(textField_price);
		textField_price.setColumns(10);

		textField_stock = new JTextField();
		textField_stock.setBounds(634, 237, 101, 20);
		frame.getContentPane().add(textField_stock);
		textField_stock.setColumns(10);

		JButton btnAddProduct = new JButton("Add");
		btnAddProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Product prod = new Product();
					prod.setDescription(textField_descr_prod.getText());
					prod.setName(textField_name_prod.getText());
					prod.setStock(Integer.parseInt(textField_stock.getText()));
					prod.setPrice(Double.parseDouble(textField_price.getText()));
					String command = new String("AddNewProduct");
					List<Object> toSend = new ArrayList<Object>();
					toSend.add(command);
					toSend.add(prod);
					SendInfoToServer.processInformation(toSend);
					update_products();
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnAddProduct.setBounds(597, 270, 123, 30);
		btnAddProduct.setIcon(iconAdd);
		frame.getContentPane().add(btnAddProduct);

		JButton btnNewButton = new JButton("Delete");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int row = table_products.getSelectedRow();
					Product product = new Product();
					product.setId((Integer) table_products.getModel().getValueAt(row, 0));
					String command = new String("DeleteProduct");
					List<Object> toSend = new ArrayList<Object>();
					toSend.add(command);
					toSend.add(product);
					SendInfoToServer.processInformation(toSend);
					update_products();
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(596, 311, 124, 30);
		btnNewButton.setIcon(iconDelete);
		frame.getContentPane().add(btnNewButton);

		JButton btnUpdateProd = new JButton("Update");
		btnUpdateProd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Product product = new Product();
					product.setId(Integer.parseInt(textField_update_id.getText()));
					product.setDescription(textField_descr_prod.getText());
					product.setName(textField_name_prod.getText());
					product.setStock(Integer.parseInt(textField_stock.getText()));
					product.setPrice(Double.parseDouble(textField_price.getText()));
					List<Object> toSend = new ArrayList<Object>();
					String command = new String("UpdateProduct");
					toSend.add(command);
					toSend.add(product);
					SendInfoToServer.processInformation(toSend);
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnUpdateProd.setBounds(597, 352, 123, 30);
		btnUpdateProd.setIcon(iconUpdate);
		frame.getContentPane().add(btnUpdateProd);

		JLabel lblId = new JLabel("ID");
		lblId.setBounds(573, 393, 46, 14);
		frame.getContentPane().add(lblId);

		textField_update_id = new JTextField();
		textField_update_id.setBounds(607, 393, 86, 20);
		frame.getContentPane().add(textField_update_id);
		textField_update_id.setColumns(10);

		JLabel lblManageProducts = new JLabel("Manage Products");
		lblManageProducts.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		lblManageProducts.setBounds(573, 128, 162, 20);
		frame.getContentPane().add(lblManageProducts);

		JLabel lblManageEmployee = new JLabel("Manage Employee");
		lblManageEmployee.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		lblManageEmployee.setBounds(830, 128, 162, 20);
		frame.getContentPane().add(lblManageEmployee);

		JLabel lblName_1 = new JLabel("Name");
		lblName_1.setBounds(798, 159, 46, 14);
		frame.getContentPane().add(lblName_1);

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(787, 184, 68, 14);
		frame.getContentPane().add(lblUsername);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(787, 215, 68, 14);
		frame.getContentPane().add(lblPassword);

		textField_employee_name = new JTextField();
		textField_employee_name.setBounds(876, 156, 138, 20);
		frame.getContentPane().add(textField_employee_name);
		textField_employee_name.setColumns(10);

		textField_employee_username = new JTextField();
		textField_employee_username.setBounds(876, 181, 138, 20);
		frame.getContentPane().add(textField_employee_username);
		textField_employee_username.setColumns(10);

		textField_employee_password = new JTextField();
		textField_employee_password.setBounds(876, 212, 138, 20);
		frame.getContentPane().add(textField_employee_password);
		textField_employee_password.setColumns(10);

		JLabel lblRole = new JLabel("Role");
		lblRole.setBounds(787, 240, 46, 14);
		frame.getContentPane().add(lblRole);

		textField_role = new JTextField();
		textField_role.setBounds(876, 237, 138, 20);
		frame.getContentPane().add(textField_role);
		textField_role.setColumns(10);

		JButton button = new JButton("Add");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					User user = new User();
					user.setName(textField_employee_name.getText());
					user.setPassword(textField_employee_password.getText());
					user.setRole(textField_role.getText());
					user.setUsername(textField_employee_username.getText());
					List<Object> toSend = new ArrayList<Object>();
					String command = new String("AddUser");
					toSend.add(command);
					toSend.add(user);
					SendInfoToServer.processInformation(toSend);
					update_employee();
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		button.setBounds(864, 274, 123, 30);
		button.setIcon(iconAdd);
		frame.getContentPane().add(button);

		JButton button_1 = new JButton("Delete");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int row = table_clients.getSelectedRow();
					User user = new User();
					user.setId((Integer) table_clients.getModel().getValueAt(row, 0));
					String command = new String("DeleteUser");
					List<Object> toSend = new ArrayList<Object>();
					toSend.add(command);
					toSend.add(user);
					SendInfoToServer.processInformation(toSend);
					update_employee();
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		button_1.setBounds(863, 315, 124, 30);
		button_1.setIcon(iconDelete);
		frame.getContentPane().add(button_1);

		JButton button_2 = new JButton("Update");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					User user = new User();
					user.setName(textField_employee_name.getText());
					user.setPassword(textField_employee_password.getText());
					user.setRole(textField_role.getText());
					user.setUsername(textField_employee_username.getText());
					user.setId(Integer.parseInt(textField_employe_ID_update.getText()));
					List<Object> toSend = new ArrayList<Object>();
					String command = new String("UpdateUser");
					toSend.add(command);
					toSend.add(user);
					SendInfoToServer.processInformation(toSend);
					update_employee();
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		button_2.setBounds(864, 356, 123, 30);
		button_2.setIcon(iconUpdate);
		frame.getContentPane().add(button_2);

		JLabel lblId_1 = new JLabel("ID");
		lblId_1.setBounds(815, 393, 46, 14);
		frame.getContentPane().add(lblId_1);

		textField_employe_ID_update = new JTextField();
		textField_employe_ID_update.setBounds(854, 390, 138, 20);
		frame.getContentPane().add(textField_employe_ID_update);
		textField_employe_ID_update.setColumns(10);

		JButton btnNewButton_1 = new JButton("PDF Report");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					List<Object> toSend = new ArrayList<Object>();
					String command = new String("GeneratePDF");
					toSend.add(command);
					toSend.add(sesionUser);
					SendInfoToServer.processInformation(toSend);
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_1.setIcon(iconPdf);
		btnNewButton_1.setBounds(681, 526, 119, 39);
		frame.getContentPane().add(btnNewButton_1);

		JLabel lblReportOfAll = new JLabel("Report of all products out of stock");
		lblReportOfAll.setBounds(649, 495, 243, 20);
		frame.getContentPane().add(lblReportOfAll);

		update_products();
		update_employee();
	}

	public void update_products() throws UnknownHostException, IOException {

		String[] columns_prod = { "Id", "Name", "Description", "Price", "Stock", };
		Object[][] dataprod = null;
		JTable table_products = new JTable(new DefaultTableModel(dataprod, columns_prod));
		table_products.setFont(new Font("Arial", Font.ITALIC, 12));
		table_products.setBackground(Color.LIGHT_GRAY);
		table_products.setForeground(Color.BLUE);
		table_products.setColumnSelectionAllowed(true);
		table_products.setCellSelectionEnabled(true);
		DefaultTableModel model_soferi_table = (DefaultTableModel) table_products.getModel();

		String command = new String("GetAllProducts");
		List<Object> toSend = new ArrayList<Object>();
		toSend.add(command);
		List<Object> allProducts = new ArrayList<Object>();

		allProducts = SendInfoToServer.processInformation(toSend);

		for (int i = 0; i < allProducts.size(); i++) {
			model_soferi_table.addRow(new Object[] { ((Product) allProducts.get(i)).getId(),
					((Product) allProducts.get(i)).getName(), ((Product) allProducts.get(i)).getDescription(),
					((Product) allProducts.get(i)).getPrice(), ((Product) allProducts.get(i)).getStock() });
		}
		table_products.setModel(model_soferi_table);
		this.table_products = table_products;
		Font f = new Font("Arial", Font.BOLD, 14);
		table_products.getTableHeader().setFont(f);
		scrollPane_products.setViewportView(table_products);
	}

	public void update_employee() throws UnknownHostException, IOException {

		String[] columns_user = { "Id", "Name", "Username", "Password", "Role" };
		Object[][] data_user = null;
		JTable table_clients = new JTable(new DefaultTableModel(data_user, columns_user));
		table_clients.setColumnSelectionAllowed(true);
		table_clients.setCellSelectionEnabled(true);
		table_clients.setFont(new Font("Arial", Font.ITALIC, 12));
		table_clients.setForeground(Color.BLUE);
		table_clients.setBackground(Color.LIGHT_GRAY);
		DefaultTableModel model_soferi_table = (DefaultTableModel) table_clients.getModel();

		String command = new String("GetAllUsers");
		List<Object> toSend = new ArrayList<Object>();
		toSend.add(command);
		List<Object> allUsers = new ArrayList<Object>();
		allUsers = SendInfoToServer.processInformation(toSend);

		for (int i = 0; i < allUsers.size(); i++) {
			if (((User) allUsers.get(i)).getId() != 2) {
				model_soferi_table.addRow(new Object[] { ((User) allUsers.get(i)).getId(),
						((User) allUsers.get(i)).getName(), ((User) allUsers.get(i)).getUsername(),
						((User) allUsers.get(i)).getPassword(), ((User) allUsers.get(i)).getRole() });
			}

		}
		table_clients.setModel(model_soferi_table);
		this.table_clients = table_clients;
		Font f = new Font("Arial", Font.BOLD, 14);
		table_clients.getTableHeader().setFont(f);
		scrollPane_employees.setViewportView(table_clients);
	}
}
