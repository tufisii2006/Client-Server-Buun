package gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.*;

import javax.swing.*;

import clientServerCommunication.SendInfoToServer;
import model.User;

public class LoginView {

	private JFrame frame;
	private JTextField txtUser;
	private JPasswordField txtPass;

	public LoginView() {

		initializeLogin();
		this.frame.setResizable(false);
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);

	}

	private void initializeLogin() {
		frame = new JFrame("Login");
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		frame.setBounds(100, 100, 533, 400);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblLogin = new JLabel("Login");
		ImageIcon Icon= new ImageIcon(this.getClass().getResource("/user-login-icon.png"));		
		lblLogin.setIcon(Icon);
		//lblLogin.setIcon(new ImageIcon("src/main/resources/photos/user-login-icon.png"));
		
		
		lblLogin.setFont(new Font("Simplified Arabic Fixed", Font.BOLD, 37));
		lblLogin.setBounds(124, 11, 295, 148);
		frame.getContentPane().add(lblLogin);

		txtUser = new JTextField();
		txtUser.setBounds(198, 170, 133, 20);
		frame.getContentPane().add(txtUser);
		txtUser.setColumns(10);

		JButton btnLogin = new JButton("Login");
		btnLogin.setIcon(new ImageIcon(this.getClass().getResource("/about-meNou.png")));
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String username = txtUser.getText();
					String password = txtPass.getText();
					if(username.equals("") || password.equals("")) {
						JOptionPane.showMessageDialog(frame, "Introduce both username and password", "LogIn ERROR",
								JOptionPane.ERROR_MESSAGE);
					}
					else {
						List<Object> toSend = new ArrayList<Object>();
						String command = new String("DoLogin");
						toSend.add(command);
						toSend.add(username);
						toSend.add(password);
						List<Object> receive = new ArrayList<Object>();
						receive = SendInfoToServer.processInformation(toSend);
						if (receive.get(0).equals("Ok") && receive.get(1).equals("admin")) {
							new AdminView((User) receive.get(2));
							frame.setVisible(false);
						} else if (receive.get(0).equals("Ok") && receive.get(1).equals("cashier")) {
							new CashierView((User) receive.get(2));
							frame.setVisible(false);
						} else {
							JOptionPane.showMessageDialog(frame, "Username or password invalid", "LogIn ERROR",
									JOptionPane.ERROR_MESSAGE);
						}
	
					}
			
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});

		btnLogin.setBounds(66, 264, 153, 59);
		frame.getContentPane().add(btnLogin);

		JButton btnExit = new JButton("Exit");
		btnExit.setIcon(new ImageIcon(this.getClass().getResource("/closeNou.png")));
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setBounds(331, 264, 133, 59);
		frame.getContentPane().add(btnExit);

		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblUsername.setBounds(114, 173, 74, 14);
		frame.getContentPane().add(lblUsername);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPassword.setBounds(114, 204, 74, 14);
		frame.getContentPane().add(lblPassword);

		txtPass = new JPasswordField();
		txtPass.setBounds(198, 201, 133, 20);
		frame.getContentPane().add(txtPass);
	}
}