package businessLogic;

import java.util.List;
import model.*;
import repository.*;

public class UserLogic {
	private UserRepo repo = new UserRepo();

	public void insertUser(User user) {
		try {
			repo.insertUser(user);

		} catch (Exception ex) {
			ex.getMessage();
		}
	}

	public void updateUser(User user) {
		try {
			repo.updateUser(user);

		} catch (Exception ex) {
			ex.getMessage();
		}
	}

	public void deleteUser(User user) {
		try {
			repo.deleteUser(user);

		} catch (Exception ex) {
			ex.getMessage();
		}
	}

	public User readUser(int id) {
		try {
			return repo.readUser(id);

		} catch (Exception ex) {
			ex.getMessage();
		}
		return null;
	}
	public List<User> viewAllUsers(){
		try {
		return	repo.viewaAllUsers();

		} catch (Exception ex) {
			ex.getMessage();
		}
		return null;
	}
	
	public User getUserByName(String name) {
		try {
			return repo.getUserByName(name);

		} catch (Exception ex) {
			ex.getMessage();
		}
		return null;
	}

}
