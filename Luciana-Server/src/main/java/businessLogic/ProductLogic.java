package businessLogic;

import java.util.List;

import model.Product;
import repository.ProductRepo;

public class ProductLogic {
	
	private  ProductRepo repo= new ProductRepo();
	
	public void insertProduct(Product product) {
		try {
			if(product==null) {
				System.out.println("The product is null");
			}
			else {
				repo.insertProduct(product);
			}			
		}
		catch(Exception ex) {
			ex.getMessage();
		}
	}
	public void deleteProduct(Product product) {
		try {
			repo.deleteProduct(product);
		}
		catch(Exception ex) {
			ex.getMessage();
		}
		
	}
	public List<Product> getAllProductsWithZeroStock(){
		return repo.getAllProductsWithZeroStock();
	}
	public void updateProduct(Product product) {
	try {
		repo.updateProduct(product);
		}
		catch(Exception ex) {
			ex.getMessage();
		}
	}
	public Product readProduct(int id) {
	try {
			Product prod= repo.readProduct(id);
			return prod;
		}
		catch(Exception ex) {
			ex.getMessage();
		}
	return null;
	}
	public List<Product> viewAllProd(){
	try {
			return repo.viewaAllProd();
		}
		catch(Exception ex) {
			ex.getMessage();
		}
	return null;
	}

}
