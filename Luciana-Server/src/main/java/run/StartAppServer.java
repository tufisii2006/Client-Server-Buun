package run;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.persistence.*;

import businessLogic.*;
import model.*;
import pdfWriter.PDFExport;

public class StartAppServer extends Thread {

	private static ProductLogic prodLogic = new ProductLogic();
	private static UserLogic userLogic = new UserLogic();

	public static void main(String[] args) throws Exception {
		@SuppressWarnings("resource")
		ServerSocket serverSocket = new ServerSocket(9999);
		System.out.println("Server started");

		/*
		 * EntityManagerFactory entityManagerFactory = Persistence
		 * .createEntityManagerFactory("org.hibernate.tutorial.jpa");
		 * 
		 * EntityManager eniEntityManager = entityManagerFactory.createEntityManager();
		 */

		while (true) {

			List<Object> receive = new ArrayList<Object>();
			// Wait for client to connect
			Socket aNewClientSocket = serverSocket.accept();
			/*
			 * Thread th = new Thread(); th.start();
			 */

			System.out.println("Server-Client connexion established!!");

			ObjectInputStream toReceiveFromClient = new ObjectInputStream(aNewClientSocket.getInputStream());
			receive = (List<Object>) toReceiveFromClient.readObject();

			System.out.println(aNewClientSocket.getLocalPort());
			System.out.println(aNewClientSocket.getInetAddress());

			List<Object> toSend;
			ObjectOutputStream objectWayToSend;

			switch ((String) receive.get(0)) {
			case "GetAllProducts":
				toSend = new ArrayList<Object>();
				List<Product> allProducts = new ArrayList<Product>();
				allProducts = prodLogic.viewAllProd();
				for (Product p : allProducts) {
					toSend.add((Product) p);
				}
				objectWayToSend = new ObjectOutputStream(aNewClientSocket.getOutputStream());
				objectWayToSend.writeObject(toSend);

				objectWayToSend.flush();
				objectWayToSend.close();
				aNewClientSocket.close();
				toReceiveFromClient.close();
				break;

			case "GetAllUsers":
				toSend = new ArrayList<Object>();
				List<User> allUsers = new ArrayList<User>();
				allUsers = userLogic.viewAllUsers();
				for (User p : allUsers) {
					toSend.add((User) p);
				}
				objectWayToSend = new ObjectOutputStream(aNewClientSocket.getOutputStream());
				objectWayToSend.writeObject(toSend);

				objectWayToSend.flush();
				objectWayToSend.close();
				aNewClientSocket.close();
				toReceiveFromClient.close();
				break;
			case "AddNewProduct":
				toSend = new ArrayList<Object>();
				Product prod = (Product) receive.get(1);
				prodLogic.insertProduct(prod);
				objectWayToSend = new ObjectOutputStream(aNewClientSocket.getOutputStream());
				objectWayToSend.writeObject(toSend);

				objectWayToSend.flush();
				objectWayToSend.close();
				aNewClientSocket.close();
				toReceiveFromClient.close();
				break;
			case "UpdateProductByCashier":
				toSend = new ArrayList<Object>();
				Product cashProd = (Product) receive.get(1);
				prodLogic.updateProduct(cashProd);
				objectWayToSend = new ObjectOutputStream(aNewClientSocket.getOutputStream());
				objectWayToSend.writeObject(toSend);

				objectWayToSend.flush();
				objectWayToSend.close();
				aNewClientSocket.close();
				toReceiveFromClient.close();
				break;
			case "FinishOrder":
				toSend = new ArrayList<Object>();
				for (Object o : receive) {
					if (o instanceof Product) {
						System.out.println(((Product) o).getId());
						prodLogic.updateProduct((Product) o);
					}
				}

				objectWayToSend = new ObjectOutputStream(aNewClientSocket.getOutputStream());
				objectWayToSend.writeObject(toSend);

				objectWayToSend.flush();
				objectWayToSend.close();
				aNewClientSocket.close();
				toReceiveFromClient.close();
				break;

			case "DeleteProduct":
				toSend = new ArrayList<Object>();
				Product p = (Product) receive.get(1);
				prodLogic.deleteProduct(p);
				objectWayToSend = new ObjectOutputStream(aNewClientSocket.getOutputStream());
				objectWayToSend.writeObject(toSend);

				objectWayToSend.flush();
				objectWayToSend.close();
				aNewClientSocket.close();
				toReceiveFromClient.close();
				break;
			case "UpdateProduct":
				toSend = new ArrayList<Object>();
				Product pr = (Product) receive.get(1);
				prodLogic.updateProduct(pr);
				objectWayToSend = new ObjectOutputStream(aNewClientSocket.getOutputStream());
				objectWayToSend.writeObject(toSend);

				objectWayToSend.flush();
				objectWayToSend.close();
				aNewClientSocket.close();
				toReceiveFromClient.close();
				break;
			case "AddUser":
				toSend = new ArrayList<Object>();
				User user = (User) receive.get(1);
				userLogic.insertUser(user);
				objectWayToSend = new ObjectOutputStream(aNewClientSocket.getOutputStream());
				objectWayToSend.writeObject(toSend);

				objectWayToSend.flush();
				objectWayToSend.close();
				aNewClientSocket.close();
				toReceiveFromClient.close();
				break;
			case "DeleteUser":
				toSend = new ArrayList<Object>();
				User use = (User) receive.get(1);
				userLogic.deleteUser(use);
				objectWayToSend = new ObjectOutputStream(aNewClientSocket.getOutputStream());
				objectWayToSend.writeObject(toSend);

				objectWayToSend.flush();
				objectWayToSend.close();
				aNewClientSocket.close();
				toReceiveFromClient.close();
				break;
			case "UpdateUser":
				toSend = new ArrayList<Object>();
				User u = (User) receive.get(1);
				userLogic.updateUser(u);
				objectWayToSend = new ObjectOutputStream(aNewClientSocket.getOutputStream());
				objectWayToSend.writeObject(toSend);

				objectWayToSend.flush();
				objectWayToSend.close();
				aNewClientSocket.close();
				toReceiveFromClient.close();
				break;
			case "DoLogin":
				toSend = new ArrayList<Object>();
				String username = (String) receive.get(1);
				String password = (String) receive.get(2);
				User loginUser = userLogic.getUserByName(username);
				if (password.equals(loginUser.getPassword())) {
					toSend.add("Ok");
					toSend.add(loginUser.getRole());
					toSend.add(loginUser);
				} else {
					toSend.add("Not Ok");
					toSend.add(loginUser.getRole());
					toSend.add(loginUser);
				}
				objectWayToSend = new ObjectOutputStream(aNewClientSocket.getOutputStream());
				objectWayToSend.writeObject(toSend);

				objectWayToSend.flush();
				objectWayToSend.close();
				aNewClientSocket.close();
				toReceiveFromClient.close();
				break;
			case "GeneratePDF":
				toSend = new ArrayList<Object>();
				User receiveUser = (User) receive.get(1);
				ArrayList<Product> zeroStockProducts = new ArrayList<Product>();
				zeroStockProducts = (ArrayList<Product>) prodLogic.getAllProductsWithZeroStock();
				PDFExport exp = new PDFExport();
				exp.setZeroStockProd(zeroStockProducts);
				exp.setUser(receiveUser);
				exp.writePDF();
				System.out.println("I wrote pdf");
				objectWayToSend = new ObjectOutputStream(aNewClientSocket.getOutputStream());
				objectWayToSend.writeObject(toSend);

				objectWayToSend.flush();
				objectWayToSend.close();
				aNewClientSocket.close();
				toReceiveFromClient.close();
				break;
			  default: System.out.println("INVALID COMAND");;
              break;
			}
		}

	}
}
